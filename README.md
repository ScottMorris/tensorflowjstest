A janky sample created based on [TensorFlow JS Tutorial - Build a neural network with TensorFlow for Beginners](https://www.youtube.com/watch?v=XdErOpUzupY).

It uses randomly generated data, which doesn't fit very well when making the model.

# Build

## Install Packages

```bash
npm install
```

## Build Project

```bash
tsc
```

# Run

## Generate Test Data

```bash
node lib/generate
```

## Run ML

```bash
node lib/ml
```
