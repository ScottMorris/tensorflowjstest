import * as fs from 'fs-extra';

export interface Customer {
	name: string;
	age: number;
	numOfPurchases: number;
	averagePurchaseAmount: number;
	hasCreditCard: boolean;
}

class TestData {
	private range(size: number, startAt: number = 0): ReadonlyArray<number> {
		return [...Array(size).keys()].map(i => i + startAt);
	}

	private getRandomArbitrary(min: number, max: number): number {
		return Math.random() * (max - min) + min;
	}
	private getRandomInt(min: number, max: number): number {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
	}

	generate(range: number = 100) {
		return this.range(range).map<Customer>(i => {
			const age = this.getRandomInt(6, 100);
			return {
				age: age,
				averagePurchaseAmount: this.getRandomArbitrary(0.05, 200),
				hasCreditCard: age > 18 ? Math.random() > 0.4 : false,
				name: `Customer ${i}`,
				numOfPurchases: this.getRandomInt(1, 50)
			};
		});
	}

	writeData(filePath: string, data: any) {
		return fs.writeJSON(filePath, data);
	}
}

const testData = new TestData();
const trainingData = testData.generate(10000);
//console.log(trainingData);
testData.writeData('./training.json', trainingData);
