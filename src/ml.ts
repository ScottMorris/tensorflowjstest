import * as tf from '@tensorflow/tfjs';
import '@tensorflow/tfjs-node';
import * as data from './training.json';
import { Customer } from './generator.js';

const customerData = <Customer[]>data;

// convert/setup data

const convertCustomer = (customer: Customer): Array<number> => [
	customer.age,
	customer.averagePurchaseAmount,
	customer.numOfPurchases
];

// split data
const trainingData = customerData.filter((v, i) => i < 80).map(c => convertCustomer(c));
const trainingDataLabels = customerData.filter((v, i) => i < 80).map(c => [c.hasCreditCard ? 1.0 : 0.0]);
const testingData = customerData.filter((v, i) => i >= 80).map(c => convertCustomer(c));
const testingDataLabels = customerData.filter((v, i) => i >= 80).map(c => [c.hasCreditCard ? 1.0 : 0.0]);

const trainingDataTf = tf.tensor2d(trainingData);
const trainingDataLabelsTf = tf.tensor2d(trainingDataLabels);
const testingDataTf = tf.tensor2d(testingData);
const testingDataLabelsTf = tf.tensor2d(testingDataLabels);

// build neural network
const model = tf.sequential();
model.add(tf.layers.dense({ inputShape: [3], activation: 'relu', units: 5 }));
model.add(tf.layers.dense({ inputShape: [5], activation: 'relu', units: 5 }));
model.add(tf.layers.dense({ activation: 'sigmoid', units: 1 }));
model.compile({ loss: 'meanSquaredError', optimizer: tf.train.adam(0.06) });

// train/fit network
model.summary();

const startTime = Date.now();
model
	.fit(trainingDataTf, trainingDataLabelsTf, { epochs: 20 })
	//.then(history => console.log('Done!', Date.now() - startTime));
	// .then(history => console.log(history));
	.then(history => model.evaluate(testingDataTf, testingDataLabelsTf))
	.then(results => results.print())
	.then(x => model.predict(tf.tensor2d([[45, 30, 2]])).print());
// Test network
